import time
import pytest
import threading
import socket
from app import app
from app.ais_dispatch import AISDispatcher

AIS_HOST = "127.0.0.1"
AIS_PORT = 10023
DATA_MAX = 1000
INTERVAL = 1
REMOTE_HOST = "https://example.com/ais/add"
REMOTE_USER = "user"
REMOTE_PW = "password"
BEARER_TOKEN = "<PASSWORD>"

# todo would be nice to have integration tests
#  that trigger the /ais endpoint and check if the correct data is collected
#  but handling the different threads is not trivial
#  the below implementation does not work because the TCP connection of the dispatcher
#  never sees the connection of the FakeAISServer

class FakeAISServer:
    def __init__(self):
        self.server_socket = None
        self.thread = None

    def __enter__(self):
        self.thread = threading.Thread(target=self.start_fake_server, daemon=True)
        self.thread.start()
        return self

    def __exit__(self, *args):
        self.server_socket.close()
        self.thread.join()

    def start_fake_server(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # Allow reuse
        self.server_socket.bind((AIS_HOST, AIS_PORT))
        self.server_socket.listen(1)

        conn, _ = self.server_socket.accept()
        with conn:
            conn.sendall(self.get_fake_data())

    @staticmethod
    def get_fake_data():
        with open("tests/nmea-sample", "rb") as f:
            data = f.read().split(b"\n")
        return data[0]


@pytest.fixture
def client():
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        yield client


def test_send_to_remote(client, requests_mock):
    requests_mock.post(
        REMOTE_HOST,
        json={
            "message": "success",
            "access_token": BEARER_TOKEN,
        },
        status_code=200,
    )
    data = {
        "ais_host_ip": AIS_HOST,
        "ais_port": AIS_PORT,
        "data_max": DATA_MAX,
        "dispatch_interval": INTERVAL,
        "remote_server_address": REMOTE_HOST,
        "remote_server_user": REMOTE_USER,
        "remote_server_password": REMOTE_PW,
    }
    response = client.post("/ais", data=data, follow_redirects=True)
    assert response.status_code == 200

    assert requests_mock.called
    assert requests_mock.request_history[0].url == REMOTE_HOST

    # test sending interval
    number_of_calls = len(requests_mock.request_history)
    time.sleep(2 * INTERVAL)
    assert len(requests_mock.request_history) == number_of_calls + 2

    # test authentication
    assert (
        requests_mock.request_history[-1].headers["Authorization"]
        == f"Bearer {BEARER_TOKEN}"
    )


def test_process_ais_data(requests_mock):
    requests_mock.post(
        REMOTE_HOST,
        json={
            "message": "success",
            "access_token": BEARER_TOKEN,
        },
        status_code=200,
    )
    args = {
        "ais_host": AIS_HOST,
        "ais_port": AIS_PORT,
        "data_max": DATA_MAX,
        "interval": INTERVAL,
        "remote_host": REMOTE_HOST,
        "remote_user": REMOTE_USER,
        "remote_pw": REMOTE_PW,
    }
    dispatcher = AISDispatcher(**args)
    with FakeAISServer() as server:
        listener = threading.Thread(target=dispatcher.ais_listener(), daemon=True)
        listener.start()
        time.sleep(1)
        assert server.get_fake_data() == dispatcher.buffer.get_current()

