from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField
from wtforms.validators import DataRequired


class AISDispatcherForm(FlaskForm):
    ais_host_ip = StringField("AIS Host IP", validators=[DataRequired()])
    ais_port = IntegerField("AIS Port", validators=[DataRequired()])
    data_max = IntegerField("Maximum Data")
    dispatch_interval = IntegerField("Interval in s", validators=[DataRequired()])
    remote_server_address = StringField(
        "Remote Server Address", validators=[DataRequired()]
    )
    remote_server_user = StringField("Remote Server User", validators=[DataRequired()])
    remote_server_password = StringField(
        "Remote Server Password", validators=[DataRequired()]
    )
    start_dispatch = SubmitField("Start")


class DispatchActiveForm(FlaskForm):
    stop_dispatch = SubmitField("Stop")
