from app import app
from flask import render_template, jsonify, redirect
from app.forms import AISDispatcherForm, DispatchActiveForm
from app.ais_dispatch import AISDispatcher
from threading import Thread


dispatcher = None


@app.route("/", methods=["GET", "POST"])
@app.route("/ais", methods=["GET", "POST"])
def ais():
    global dispatcher
    config_form = AISDispatcherForm()
    if dispatcher and dispatcher.active:
        return redirect("/sending")

    if config_form.validate_on_submit():
        args = {
            "ais_host": config_form.ais_host_ip.data,
            "ais_port": config_form.ais_port.data,
            "data_max": config_form.data_max.data,
            "interval": config_form.dispatch_interval.data,
            "remote_host": config_form.remote_server_address.data,
            "remote_user": config_form.remote_server_user.data,
            "remote_pw": config_form.remote_server_password.data,
        }
        try:
            dispatcher = AISDispatcher(**args)
            thread = Thread(target=dispatcher.start_dispatch)
            thread.start()
        except Exception as e:
            dispatcher = None
            return jsonify({"error": str(e)}), 500
        return redirect("/sending")
    return render_template("ais.html", title="AIS Dispatcher", form=config_form)


@app.route("/sending", methods=["GET", "POST"])
def sending():
    global dispatcher
    if not dispatcher or (dispatcher and not dispatcher.active):
        return redirect("/")
    if dispatcher and dispatcher.error:
        return jsonify({"error": str(dispatcher.error)}), 500

    dispatch_active_form = DispatchActiveForm()
    if dispatch_active_form.validate_on_submit():
        dispatcher.stop_dispatch()
        return redirect("/")
    return render_template(
        "dispatching.html", title="Dispatching", form=dispatch_active_form
    )


@app.route("/data_size", methods=["GET"])
def data_size():
    global dispatcher
    return jsonify({"total_sent": dispatcher.total_data_sent() if dispatcher else 0})
