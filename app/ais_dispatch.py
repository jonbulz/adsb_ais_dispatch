import time
import csv
import io
import json

import requests
from pyais import TCPConnection
from threading import Thread
from app.ship_data_buffer import ShipDataBuffer


class AISDispatcher:
    def __init__(
        self,
        ais_host,
        ais_port,
        data_max,
        interval: int,
        remote_host,
        remote_user,
        remote_pw,
    ):
        self.ais_host = ais_host
        self.ais_port = ais_port
        self.remote_host_base_url = remote_host
        self.remote_user = remote_user
        self.remote_pw = remote_pw
        self.session = requests.Session()
        self._authenticate_session(self.session)
        self.buffer = ShipDataBuffer()

        self.active = True
        self.data_used = 0
        self.data_max = data_max
        self.interval = interval
        self.error = None

    def start_dispatch(self):
        """
        main function
        starts the ais_listener and continuously sends
        the buffered ship data to the remote host
        """
        listener = Thread(target=self.ais_listener)
        listener.daemon = True
        listener.start()
        while self.data_used < self.data_max and self.active:
            try:
                self._dispatch()
            except Exception as e:
                # todo probably better to do logging for specific exceptions here
                #  let actual exceptions bubble up to the user
                self.error = e
            if self.data_used >= self.data_max:
                self.active = False
                self.buffer.save()
                break
            if not self.active:
                break
            time.sleep(self.interval)

    def ais_listener(self):
        conn = TCPConnection(self.ais_host, self.ais_port)
        for msg in conn:
            self.buffer.update(msg)

    def _authenticate_session(self, session):
        auth_url = self.remote_host_base_url + "token/"
        headers = {"Content-Type": "application/json"}
        credentials = {"username": self.remote_user, "password": self.remote_pw}
        data = json.dumps(credentials)
        auth_response = session.post(auth_url, headers=headers, data=data)

        if auth_response.status_code == 200:
            token = auth_response.json().get("access")
            session.headers.update({"Authorization": f"Bearer {token}"})
        else:
            raise Exception(f"Failed to authenticate: {auth_response.status_code}")

    def _refresh_session(self, session):
        # todo do we have to differentiate between authentication and refresh?
        auth_url = self.remote_host_base_url + "token/"
        headers = {"Content-Type": "application/json"}
        credentials = {"username": self.remote_user, "password": self.remote_pw}
        data = json.dumps(credentials)
        auth_response = session.post(auth_url, headers=headers, data=data)

        if auth_response.status_code == 200:
            token = auth_response.json().get("refresh")
            session.headers.update({"Authorization": f"Bearer {token}"})
        else:
            raise Exception(f"Failed to refresh: {auth_response.status_code}")

    def total_data_sent(self):
        return self.data_used

    def stop_dispatch(self):
        self.active = False
        self.buffer.save()

    @staticmethod
    def _ais_to_csv(data):
        out = io.StringIO()

        # todo define cols in a central place?
        cols = ["timestamp", "identifier", "lat", "lon", "cog", "sog"]
        required = ["timestamp", "identifier", "lat", "lon"]
        writer = csv.DictWriter(out, fieldnames=cols)
        writer.writeheader()
        for identifier, vals in data.items():
            # DictWriter is strict about matching the columns
            # so we need to filter our values
            vals = {k: v for k, v in vals.items() if k in cols}
            incomplete_data = {key for key in required if not vals.get(key)}
            if incomplete_data:
                continue
            row = {"identifier": identifier, **vals}
            writer.writerow(row)
        csv_data = out.getvalue()
        out.close()
        return csv_data

    def _dispatch(self):
        """
        sends current state of ship_buffer to remote host
        """
        data = self.buffer.get_current()
        if not data:
            return
        url = f"{self.remote_host_base_url}ais/add/"
        csv_data = self._ais_to_csv(data)
        response = self.session.post(url, data=csv_data)
        self._calc_data_used(response)
        if response.status_code == 401:
            self._refresh_session(self.session)
            response = self.session.post(url, data=csv_data)
            self._calc_data_used(response)
        response.raise_for_status()
        if response.status_code == 200:
            self.buffer.clear()

    def _calc_data_used(self, response):
        headers_size = sum(
            len(k) + len(v) + 4 for k, v in response.request.headers.items()
        )
        if response.request.body:
            body_size = len(response.request.body)
        else:
            body_size = 0
        self.data_used += headers_size + body_size
