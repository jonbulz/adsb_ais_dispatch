import json
import time


class ShipDataBuffer:
    def __init__(self):
        try:
            with open("ship_buffer.json", "r") as f:
                self.data = json.load(f)
        except IOError:
            self.data = {}

    def save(self):
        with open("ship_buffer.json", "w") as f:
            f.write(json.dumps(self.get_current(), indent=4))

    def update(self, msg):
        try:
            decoded = msg.decode()
            mmsi = decoded.mmsi
            ship_data = self._get_data_as_dict(decoded)
            self.data.update({mmsi: ship_data})
        except Exception as e:
            print(f"Failed to process AIS message {msg}: {e}")

    def get_current(self):
        return {mmsi: ship_data for mmsi, ship_data in self.data.items() if self._data_filter(ship_data)}

    def clear(self):
        self.data.clear()

    @staticmethod
    def _data_filter(ship_data):
        return (
            (time.time() - ship_data["timestamp"] < 1800)
            and (ship_data.get("lat") and (32.38 < ship_data["lat"] < 36.02))
            and (ship_data.get("lon") and (11.18 < ship_data["lon"] < 14.3))
        )

    @staticmethod
    def _get_data_as_dict(msg):
        data = {}
        if msg.msg_type < 4:
            data = {
                "timestamp": int(time.time()),
                "identifier": msg.mmsi,
                "lat": round(msg.lat, 3),
                "lon": round(msg.lon, 3),
                "cog": int(round(msg.course, 0)),
                "sog": int(round(msg.speed, 0)),
            }
        if msg.msg_type == 5:
            data = {
                "timestamp": int(time.time()),
                "identifier": msg.mmsi,
                "name": msg.shipname,
                "shiptype": msg.ship_type,
            }
        return data
