import os


class Config:
    SECRET_KEY = os.environ.get("SECRET_KEY") or "very-very-secret-key"
    ADSB_AIS_SERVER_URL = os.environ.get("ADSB_AIS_SERVER_URL")
    ASSET_API_TIMEOUT = os.environ.get("ASSET_API_TIMEOUT") or 10
